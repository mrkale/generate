# generate

It is a git repository with windows command prompt script (batch file) for generating skeleton of a new Joomla 3 agenda for my component **com_gbjfamily**.


> The repository **should not** be cloned in a folder, which full path contains the word "agenda" in either case. If it does, the script renames those words in path as well and target agenda skeleton will be located in unexpected target folder.


### Prerequisities
- SED GNU utility available (on the system PATH)


### Command line
The script should be launched from the command prompt in the form

	generate>agenda <newagenda>

where

- **agenda** is the name of the script without extension `.cmd`, but it can be used
- **<newagenda>** is the placeholder for the new generating agenda

> The name of the new agenda SHOULD be written in **singular**, e.g., *Fact* with **regular plural** in order to get proper plural by replacing the word *agenda* in the word *agendas*.


### Example

    agenda fact


### Target location and actions
The script locates all files of a new agenda in the folder **target** and with the same structure as they are in the *seed* folder.

- The script renames the word **agenda** in either case in all seed subfolders on every level to the new agenda name in corresponding case and creates those new folders in the target folder.
- The script renames the word **agenda** in either case in all seed file names on every level to the new agenda name in corresponding case and creates those new files in corresponding target folder.
- The script renames the word **agenda** in either case in content of all files (especially classes names) to the new agenda name in corresponding case.
- **Multiple new agendas** can be generated one by one. The corresponding files will be located in common subfolders together (e.g., controllers, models) and in agenda specific subfolders (e.g., views) separately. 
