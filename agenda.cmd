@ECHO OFF

REM Generating agenda skeleton from the seed structure
REM Version 1.2.0
REM (c) 2018 Libor Gabaj
REM Credits: https://gist.github.com/ijprest/1207818
REM 02.05.2018 - Initial version
REM 10.05.2018 - Replaced PowerShell with internal subroutines
REM 17.04.2019 - Added current year processing for copyright clauses

TITLE Agenda Generation

SETLOCAL ENABLEEXTENSIONS ENABLEDELAYEDEXPANSION
PUSHD

REM Agenda name variations
SET target_agenda_proper=%1
SET target_agenda_upper=%target_agenda_proper%
SET target_agenda_lower=%target_agenda_proper%
CALL :toproper target_agenda_proper
CALL :toupper target_agenda_upper
CALL :tolower target_agenda_lower
::
SET seed_agenda_proper=Agenda
SET seed_agenda_upper=%seed_agenda_proper%
SET seed_agenda_lower=%seed_agenda_proper%
CALL :toproper seed_agenda_proper
CALL :toupper seed_agenda_upper
CALL :tolower seed_agenda_lower

ECHO === Start of generating agenda %target_agenda_proper% ===

REM Parameters
SET seed_tree=seed
SET seed_year=YYYY
SET target_tree=target
SET target_year=%date:~-4%

REM Create target folder structure
FOR /F "tokens=*" %%G IN ('DIR /b /s /a:d "%seed_tree%"') DO (
ECHO %%G | sed -e "s/\\%seed_tree%\\/\\%target_tree%\\/" -e "s/%seed_agenda_lower%/%target_agenda_lower%/g" > text.tmp
SET /p folder=<text.tmp
MD "!folder!" 2>NUL
)

REM Process files
FOR /R %seed_tree% %%G IN (*) DO (
ECHO %%G | sed -e "s/\\%seed_tree%\\/\\%target_tree%\\/" -e "s/%seed_agenda_lower%/%target_agenda_lower%/g" > text.tmp
SET /p file=<text.tmp
sed -e "s/%seed_agenda_proper%/%target_agenda_proper%/g" -e "s/%seed_agenda_upper%/%target_agenda_upper%/g" -e "s/%seed_agenda_lower%/%target_agenda_lower%/g" -e "s/%seed_year%/%target_year%/g" "%%G" > !file! 2>NUL
)

GOTO CLOSE

:: toupper & tolower; makes use of the fact that string 
:: replacement (via SET) is not case sensitive
:toupper
FOR %%L IN (A B C D E F G H I J K L M N O P Q R S T U V W X Y Z) DO SET %1=!%1:%%L=%%L!
GOTO :EOF

:tolower
FOR %%L IN (a b c d e f g h i j k l m n o p q r s t u v w x y z) DO SET %1=!%1:%%L=%%L!
GOTO :EOF

:toproper
CALL :tolower %1
SET cl=!%1:~0,1!
CALL :toupper cl
SET %1=!cl!!%1:~1!
GOTO :EOF

:CLOSE
DEL text.tmp 2>NUL
POPD
ECHO === End of generating agenda %target_agenda_proper% ===
REM PAUSE
